import urllib

from django.apps import AppConfig
from .ytchannel import YTChannel
from . import data

class YoutubeappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'youtubeapp'

    def ready(self):
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
              + 'UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urllib.request.urlopen(url)
        channel = YTChannel(xmlStream)
        data.selectable,data.channel = channel.videos()
